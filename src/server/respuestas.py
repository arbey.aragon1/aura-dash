##################################################1
html.Div(
    className="two columns card",
    children=[
        html.H6("Select Leverage"),
        dcc.RadioItems(
            id="leverage-select",
            options=[
                {'label': str(label), 'value': str(label)} for label in df['Margin'].unique()
            ],
            value='1',
            labelStyle={'display': 'inline-block'}
        ),
    ]
)

##################################################2
html.Div(
    className="three columns card",
    children=[
        html.H6("Select a Date Range"),
        dcc.DatePickerRange(
            id="date-range",
            display_format="MMM YY",
            start_date=df['Entry time'].min(),
            end_date=df['Entry time'].max()
        ),
    ]
)

##################################################3
@app.callback(
    [],
    (
        dash.dependencies.Input('exchange-select', 'value'),
        dash.dependencies.Input('date-range', 'start_date'),
        dash.dependencies.Input('date-range', 'end_date'),
    )
)
def update_monthly(exchange, start_date, end_date):
    df2 = df[(df['Entry time'] >= start_date) & (df['Entry time'] < end_date)]
    df3 = df2[df2['Exchange'] == exchange]
    

##################################################4

def filter_df(df, exchange, leverage, start_date, end_date):
    df2 = df[(df['Entry time'] >= start_date) & (df['Entry time'] < end_date)]
    df3 = df2[df2['Exchange'] == exchange]
    dff = df3[df3['Margin'] == int(leverage)]
    return dff


##################################################5


def calc_returns_over_month(dff):
    out = []
    if(dff.shape[0]>0):
        for name, group in dff.groupby('YearMonth'):
            exit_balance = group.head(1)['Exit balance'].values[0]
            entry_balance = group.tail(1)['Entry balance'].values[0]
            monthly_return = (exit_balance*100 / entry_balance)-100
            out.append({
                'month': name,
                'entry': entry_balance,
                'exit': exit_balance,
                'monthly_return': monthly_return
            })
    else:
        print(dff.shape)
    return out


def calc_btc_returns(dff):
    btc_returns = 0
    if(dff.shape[0]>0):
        btc_start_value = dff.tail(1)['BTC Price'].values[0]
        btc_end_value = dff.head(1)['BTC Price'].values[0]
        btc_returns = (btc_end_value * 100/ btc_start_value)-100
    else:
        print(dff.shape)
    return btc_returns

def calc_strat_returns(dff):
    returns = 0
    if(dff.shape[0]>0):
        start_value = dff.tail(1)['Exit balance'].values[0]
        end_value = dff.head(1)['Entry balance'].values[0]
        returns = (end_value * 100/ start_value)-100
    else:
        print(dff.shape)
    return returns


data = calc_returns_over_month(dff)
btc_returns = calc_btc_returns(dff)
strat_returns = calc_strat_returns(dff)
strat_vs_market = strat_returns - btc_returns

########

html.Div(
    className="twelve columns card",
    children=[
        dcc.Graph(
            id="monthly-chart",
            figure={
                'data': []
            }
        )
    ]
),


##################################################6

@app.callback(
    [
        dash.dependencies.Output('monthly-chart', 'figure'),
    ],
    (
        dash.dependencies.Input('exchange-select', 'value'),
        dash.dependencies.Input('leverage-select', 'value'),
        dash.dependencies.Input('date-range', 'start_date'),
        dash.dependencies.Input('date-range', 'end_date'),

    )
)
def update_monthly(exchange, leverage, start_date, end_date):
    dff = filter_df(df, exchange, leverage, start_date, end_date)
    data = calc_returns_over_month(dff)
    btc_returns = calc_btc_returns(dff)
    strat_returns = calc_strat_returns(dff)
    strat_vs_market = strat_returns - btc_returns

    return {
        'data': [
            go.Candlestick(
                open=[each['entry'] for each in data],
                close=[each['exit'] for each in data],
                x=[each['month'] for each in data],
                low=[each['entry'] for each in data],
                high=[each['exit'] for each in data]
            )
        ],
        'layout': {
            'title': 'Overview of Monthly performance'
        }
    }

