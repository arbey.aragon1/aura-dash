import pandas as pd
import dash
import dash_core_components as dcc
import dash_html_components as html
import dash_table
import plotly.graph_objects as go
import sys
import os

df = pd.read_csv('aggr.csv', parse_dates=['Entry time'])
df['YearMonth'] = pd.to_datetime(df['Entry time'].map(lambda x: "{}-{}".format(x.year, x.month)))

app = dash.Dash(__name__, external_stylesheets=['https://codepen.io/uditagarwal/pen/oNvwKNP.css', 'https://codepen.io/uditagarwal/pen/YzKbqyV.css'])
app.layout = html.Div(children=[
    html.Div(
            children=[
                html.H2(children="Bitcoin Leveraged Trading Backtest Analysis", className='h2-title'),
            ],
            className='study-browser-banner row'
    ),
    html.Div(
        className="row app-body",
        children=[
            html.Div(
                className="twelve columns card",
                children=[
                    html.Div(
                        className="padding row",
                        children=[
                            html.Div(
                                className="two columns card",
                                children=[
                                    html.H6("Select Exchange",),
                                    dcc.RadioItems(
                                        id="exchange-select",
                                        options=[
                                            {'label': label, 'value': label} for label in df['Exchange'].unique()
                                        ],
                                        value='Bitmex',
                                        labelStyle={'display': 'inline-block'}
                                    )
                                ]
                            ),
                            html.Div(
                                className="two columns card",
                                children=[
                                    html.H6("Select Leverage"),
                                    dcc.RadioItems(
                                        id="leverage-select",
                                        options=[
                                            {'label': str(label), 'value': str(label)} for label in df['Margin'].unique()
                                        ],
                                        value='1',
                                        labelStyle={'display': 'inline-block'}
                                    ),
                                ]
                            ),
                            html.Div(
                                className="three columns card",
                                children=[
                                    html.H6("Select a Date Range"),
                                    dcc.DatePickerRange(
                                        id="date-range",
                                        display_format="MMM YY",
                                        start_date=df['Entry time'].min(),
                                        end_date=df['Entry time'].max()
                                    ),
                                ]
                            ),
                            html.Div(
                                id="strat-returns-div",
                                className="two columns indicator pretty_container",
                                children=[
                                    html.P(id="strat-returns", className="indicator_value"),
                                    html.P('Strategy Returns', className="twelve columns indicator_text"),
                                ]
                            ),
                            html.Div(
                                id="market-returns-div",
                                className="two columns indicator pretty_container",
                                children=[
                                    html.P(id="market-returns", className="indicator_value"),
                                    html.P('Market Returns', className="twelve columns indicator_text"),
                                ]
                            ),
                            html.Div(
                                id="strat-vs-market-div",
                                className="two columns indicator pretty_container",
                                children=[
                                    html.P(id="strat-vs-market", className="indicator_value"),
                                    html.P('Strategy vs. Market Returns', className="twelve columns indicator_text"),
                                ]
                            ),
                        ]
                )
        ]),
        html.Div(
            className="twelve columns card",
            children=[
                dcc.Graph(
                    id="monthly-chart",
                    figure={
                        'data': []
                    }
                )
            ]
        ),
        html.Div(
                className="padding row",
                children=[
                    html.Div(
                        className="six columns card",
                        children=[
                            dash_table.DataTable(
                                id='table',
                                columns=[
                                    {'name': 'Number', 'id': 'Number'},
                                    {'name': 'Trade type', 'id': 'Trade type'},
                                    {'name': 'Exposure', 'id': 'Exposure'},
                                    {'name': 'Entry balance', 'id': 'Entry balance'},
                                    {'name': 'Exit balance', 'id': 'Exit balance'},
                                    {'name': 'Pnl (incl fees)', 'id': 'Pnl (incl fees)'},
                                ],
                                style_cell={'width': '50px'},
                                style_table={
                                    'maxHeight': '450px',
                                    'overflowY': 'scroll'
                                },
                            )
                        ]
                    ),
                    dcc.Graph(
                        id="pnl-types",
                        className="six columns card",
                        figure={
                            'data': []
                        }
                    )
                ]
            ),
            html.Div(
                className="padding row",
                children=[
                    dcc.Graph(
                        id="daily-btc",
                        className="six columns card",
                        figure={
                            'data': []  
                        }
                    ),
                    dcc.Graph(
                        id="balance",
                        className="six columns card",
                        figure={
                            'data': []
                        }
                    )
                ]
            )
        ]
    )        
])

def calc_returns_over_month(dff):
    out = []
    if(dff.shape[0]>0):
        for name, group in dff.groupby('YearMonth'):
        
            exit_balance = group.head(1)['Exit balance'].values[0]
            entry_balance = group.tail(1)['Entry balance'].values[0]
            monthly_return = (exit_balance*100 / entry_balance)-100
            out.append({
                'month': name,
                'entry': entry_balance,
                'exit': exit_balance,
                'monthly_return': monthly_return
            })
    else:
        print(dff.shape)
    return out


def calc_btc_returns(dff):
    btc_returns = 0
    if(dff.shape[0]>0):
        btc_start_value = dff.tail(1)['BTC Price'].values[0]
        btc_end_value = dff.head(1)['BTC Price'].values[0]
        btc_returns = (btc_end_value * 100/ btc_start_value)-100
    else:
        print(dff.shape)
    return btc_returns

def calc_strat_returns(dff):
    returns = 0
    if(dff.shape[0]>0):
        start_value = dff.tail(1)['Exit balance'].values[0]
        end_value = dff.head(1)['Entry balance'].values[0]
        returns = (end_value * 100/ start_value)-100
    else:
        print(dff.shape)
    return returns

def filter_df(df, exchange, leverage, start_date, end_date):
    df2 = df[(df['Entry time'] >= start_date) & (df['Entry time'] < end_date)]
    df3 = df2[df2['Exchange'] == exchange]
    dff = df3[df3['Margin'] == int(leverage)]
    return dff

@app.callback(
    [
        dash.dependencies.Output('monthly-chart', 'figure'),
        dash.dependencies.Output('market-returns', 'children'),
        dash.dependencies.Output('strat-returns', 'children'),
        dash.dependencies.Output('strat-vs-market', 'children'),
        dash.dependencies.Output('table', 'data'),
        dash.dependencies.Output('pnl-types', 'figure'),
        dash.dependencies.Output('daily-btc', 'figure'),
        dash.dependencies.Output('balance', 'figure'),
    ],
    (
        dash.dependencies.Input('exchange-select', 'value'),
        dash.dependencies.Input('leverage-select', 'value'),
        dash.dependencies.Input('date-range', 'start_date'),
        dash.dependencies.Input('date-range', 'end_date')
    )
)
def update_monthly(exchange, leverage, start_date, end_date):
    dff = filter_df(df, exchange, leverage, start_date, end_date)
    data = calc_returns_over_month(dff)
    btc_returns = calc_btc_returns(dff)
    strat_returns = calc_strat_returns(dff)
    strat_vs_market = strat_returns - btc_returns
    
    cols=['Number','Trade type','Exposure','Entry balance','Exit balance','Pnl (incl fees)']


    dff['ret'] = (dff['Exit balance']-dff['Entry balance'])/dff['Entry balance']*100

    dataShort = dff.copy()
    dataShort = dataShort[dataShort['Trade type']=='Short']
    dataLong = dff.copy()
    dataLong = dataLong[dataLong['Trade type']=='Long']

    dataInRangeTime = df[(df['Entry time'] >= start_date) & (df['Entry time'] < end_date)].copy()
    dataInRangeTime.set_index(['Entry time'])
    dataInRangeTime.sort_index(inplace=True)
    return {
            'data': [
                go.Candlestick(
                    open=[each['entry'] for each in data],
                    close=[each['exit'] for each in data],
                    x=[each['month'] for each in data],
                    low=[each['entry'] for each in data],
                    high=[each['exit'] for each in data]
                )
            ],
            'layout': {
                'title': 'Overview of Monthly performance'
            }
        }, \
        f'{btc_returns:0.2f}%', \
        f'{strat_returns:0.2f}%', \
        f'{strat_vs_market:0.2f}%', \
        dff[cols].to_dict('records'), \
        {
            'data':[
                go.Bar(
                    x=dataShort['Entry time'],
                    y=dataShort['ret'],
                    name='Short',
                    marker=go.bar.Marker(
                        color='rgb(55, 83, 109)'
                    )
                ),
                go.Bar(
                    x=dataLong['Entry time'],
                    y=dataLong['ret'],
                    name='Long',
                    marker=go.bar.Marker(
                        color='rgb(26, 118, 255)'
                    )
                )
            ],
            'layout':go.Layout(
                title='PnL vs Trade type',
                autosize=False,
                width=500,
                height=500,
                showlegend=True,
                legend=go.layout.Legend(
                    x=0,
                    y=1.0
                ),
                margin=go.layout.Margin(l=40, r=0, t=40, b=30)
            )
        }, \
        {
            'data': [
                go.Scatter(
                    x=dff['Entry time'],
                    y=dff['BTC Price'],
                )
            ],
            'layout': {
                'title': 'Daily BTC Price',
                'autosize':'False',
                'width':500,
                'height':500,
                 'margin=':{'l': 50, 'b': 50, 't': 50, 'r': 50},
            }
        }, \
        {
            'data': [
                go.Scatter(
                    x=dff['Entry time'],
                    y=dff['Entry balance'],
                )
            ],
            'layout': {
                'title': 'Balance overtime',
                'autosize':'False',
                'width':500,
                'height':500,
                 'margin=':{'l': 50, 'b': 50, 't': 50, 'r': 50},     
               }
        }, \


if __name__ == "__main__":
    app.run_server(debug=True, port=8050, host='0.0.0.0')


    fig.update_layout(
    autosize=False,
    width=500,
    height=500,
    margin=go.layout.Margin(
        l=50,
        r=50,
        b=100,
        t=100,
        pad=4
    ),
    paper_bgcolor="LightSteelBlue",
)